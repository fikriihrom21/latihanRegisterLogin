package com.example.latihanregisterlogin.api;


import com.example.latihanregisterlogin.model.login.Login;
import com.example.latihanregisterlogin.model.login.LoginData;
import com.example.latihanregisterlogin.model.register.Register;
import com.example.latihanregisterlogin.model.register.RegisterData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("login.php")
    Call<Login> loginResponse(
            @Field("username") String userName,
            @Field("password") String Password
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<Register> registerResponse(
            @Field("username") String userName,
            @Field("password") String Password,
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("address") String address

    );

}
