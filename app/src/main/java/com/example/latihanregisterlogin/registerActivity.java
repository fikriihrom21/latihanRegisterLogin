package com.example.latihanregisterlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.latihanregisterlogin.api.ApiClient;
import com.example.latihanregisterlogin.api.ApiInterface;
import com.example.latihanregisterlogin.model.register.Register;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class registerActivity extends AppCompatActivity implements View.OnClickListener{

    EditText etUserName,etPassword, etName,etEmail,etPhone, etAddress;
    Button btnRegister;
    TextView tvLogin;
    String userName, name, password, email, phone, address;
    ApiInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etUserName = findViewById(R.id.eRegisterUserName);
        etName = findViewById(R.id.eRegisterName);
        etPassword = findViewById(R.id.eRegisterPassword);
        etEmail = findViewById(R.id.eEmail);
        etPhone = findViewById(R.id.ePhone);
        etAddress = findViewById(R.id.eAddress);
        btnRegister = findViewById(R.id.btnRegister);
        tvLogin = findViewById(R.id.tvLogin);

        btnRegister.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRegister:
                userName = etUserName.getText().toString();
                name = etName.getText().toString();
                password = etPassword.getText().toString();
                email = etEmail.getText().toString();
                phone = etPhone.getText().toString();
                address = etAddress.getText().toString();
                register(userName,name, password,email,phone,address);
                break;
            case R.id.tvLogin:
                Intent intent = new Intent(this,loginActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void register(String userName,String name,String password,String email, String phone, String address) {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Register> call = apiInterface.registerResponse(userName, password, name,email, phone, address);
        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isStatus()) {
                    Toast.makeText(registerActivity.this, response.body().getRegisterData().getName(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(registerActivity.this, loginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(registerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                Toast.makeText(registerActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }
}